package pl.polak.dawid.sockets.server.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pl.polak.dawid.sockets.server.domain.Role;
import pl.polak.dawid.sockets.server.domain.User;
import pl.polak.dawid.sockets.server.repository.UserManagerBuilder;

import javax.security.auth.login.LoginException;
import java.util.Arrays;
import java.util.Collections;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class UserControlTest {

    private UserControl userControl;
    private final UserManagerBuilder userBuilder = mock(UserManagerBuilder.class);

    @BeforeEach
    void setUp() {
        userControl = new UserControl(userBuilder);
    }

    @Test
    @DisplayName("Should return true when user exists in database")
    void shouldReturnTrueWhenUserExistsInDatabase() {
        when(userBuilder.loadUsersList()).thenReturn(Arrays.asList("admin", "dawid"));

        boolean userDawidExists = userControl.userExists("dawid");

        assertThat(userDawidExists).isTrue();
    }

    @Test
    @DisplayName("Should throw LoginException when user does not exists in database")
    void shouldThrowLoginExceptionWhenUserDoesNotExistsInDatabase() {
        when(userBuilder.loadUsersList()).thenReturn(Arrays.asList("admin", "dawid"));

        assertThatExceptionOfType(LoginException.class)
                .isThrownBy(() -> userControl.loginUser("justyna", "pass"));
    }

    @Test
    @DisplayName("Should return true when registration succeeded")
    void shouldReturnTrueWhenUserRegistrationSucceeded() {
        when(userBuilder.saveUser(any(User.class))).thenReturn(true);

        boolean userIsRegistered = userControl.registerUser("test", "pass");

        assertThat(userIsRegistered).isTrue();
    }

    @Test
    @DisplayName("Should throw LoginException when password does not match")
    void shouldThrowLoginExceptionWhenPasswordDoesNotMatch() {
        when(userBuilder.loadUsersList()).thenReturn(Arrays.asList("admin", "dawid"));
        when(userBuilder.loadUserByName(anyString())).thenReturn(
                new User("dawid", "pass", Role.USER));

        assertThatExceptionOfType(LoginException.class)
                .isThrownBy(() -> userControl.loginUser("dawid", "12345"));
    }

    @Test
    @DisplayName("User instance should be not null after login succeeded")
    void shouldBeNotNullAfterLoginSucceeded() throws LoginException {
        when(userBuilder.loadUsersList()).thenReturn(Collections.singletonList("test"));
        when(userBuilder.loadUserByName("test")).thenReturn(
                new User("test", "pass", Role.USER));
        userControl.loginUser("test", "pass");

        boolean userIsLoggedIn = userControl.userWantToLoginOrRegister();

        assertThat(userIsLoggedIn).isNotNull();
    }

    @Test
    @DisplayName("Should return true when user is admin")
    void shouldReturnTrueWhenUserIsAdmin() throws LoginException {
        when(userBuilder.loadUsersList()).thenReturn(Collections.singletonList("admin"));
        when(userBuilder.loadUserByName("admin")).thenReturn(
                new User("admin", "klepacz", Role.ADMIN));

        userControl.loginUser("admin", "klepacz");
        boolean isAdminUser = userControl.isAdmin();

        assertThat(isAdminUser).isTrue();
    }

    @Test
    @DisplayName("Should return false when user is not admin")
    void shouldReturnFalseWhenUserIsNotAdmin() throws LoginException {
        when(userBuilder.loadUsersList()).thenReturn(Collections.singletonList("user"));
        when(userBuilder.loadUserByName("user")).thenReturn(
                new User("user", "pass", Role.USER));

        userControl.loginUser("user", "pass");
        boolean isAdminUser = userControl.isAdmin();

        assertThat(isAdminUser).isFalse();
    }
}