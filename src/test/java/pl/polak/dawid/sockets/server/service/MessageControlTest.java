package pl.polak.dawid.sockets.server.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import pl.polak.dawid.sockets.server.domain.Message;
import pl.polak.dawid.sockets.server.domain.Role;
import pl.polak.dawid.sockets.server.domain.User;
import pl.polak.dawid.sockets.server.repository.MessageManagerBuilder;
import pl.polak.dawid.sockets.server.repository.UserManagerBuilder;

import javax.security.auth.login.LoginException;
import java.util.Arrays;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class MessageControlTest {

    private MessageControl messageControl;
    private UserControl userControl;
    private final MessageManagerBuilder messageBuilder = mock(MessageManagerBuilder.class);
    private final UserManagerBuilder userBuilder = mock(UserManagerBuilder.class);

    @BeforeEach
    void setUp() { messageControl = new MessageControl(messageBuilder, userBuilder);
                    userControl = new UserControl(userBuilder); }

    @Test
    @DisplayName("Sending message from admin user to dawid user succeeded")
    void sendingMessageToUserSucceeded() throws LoginException {
        when(userBuilder.loadUsersList()).thenReturn(Arrays.asList("admin", "dawid"));
        when(userBuilder.loadUserByName(anyString())).thenReturn(new User(1,"admin", "pass", Role.ADMIN));
        userControl.loginUser("admin", "pass");
        when(userBuilder.loadUserByName(anyString())).thenReturn(new User(2,"dawid", "123pass", Role.USER));

        messageControl.sendMessage("dawid", "Hi dude!");
        ArgumentCaptor<Message> argument = ArgumentCaptor.forClass(Message.class);
        verify(messageBuilder).saveMessage(argument.capture());
        Message message = argument.getValue();
       assertThat(message.getBody()).isEqualTo("Hi dude!");
       assertThat(message.getOwnerId()).isEqualTo(2);
       assertThat(message.getSenderId()).isEqualTo(1);
    }
}
