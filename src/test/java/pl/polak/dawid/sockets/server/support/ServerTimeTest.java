package pl.polak.dawid.sockets.server.support;

import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pl.polak.dawid.sockets.server.service.support.ServerTime;
import pl.polak.dawid.sockets.server.service.support.TimeStarter;
import pl.polak.dawid.sockets.server.service.support.Uptime;

import java.time.Instant;
import java.time.LocalDate;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ServerTimeTest {

    private ServerTime serverTime;
    private final TimeStarter timeStarter = mock(TimeStarter.class);

    @BeforeEach
    void setUp() {
        when(timeStarter.getStartTimeOfServer()).thenReturn(Instant.parse("2021-12-15T00:00:00.00Z"));
        serverTime = new ServerTime(timeStarter);
    }

    @Test
    @DisplayName("Should return current date")
    void shouldReturnCurrentDate() {
        LocalDate currentDate = LocalDate.now();

        LocalDate startDateOfServer = serverTime.getStartDateOfServer();

        assertThat(startDateOfServer).isEqualTo(currentDate);
    }

    @Test
    @DisplayName("Life time of the server is 12 seconds")
    void shouldReturn12seconds() {
        when(timeStarter.getStartTimeOfServer()).thenReturn(Instant.parse("2021-12-15T00:00:12.00Z"));

        Uptime uptime = serverTime.getUptime();

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(uptime.getSeconds()).as("seconds").isEqualTo(12);
    }

    @Test
    @DisplayName("Life time of the server is not 12 seconds and is equal to 23 seconds")
    void shouldNotReturn12seconds() {
        when(timeStarter.getStartTimeOfServer()).thenReturn(Instant.parse("2021-12-15T00:00:23.00Z"));

        Uptime uptime = serverTime.getUptime();

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(uptime.getSeconds()).as("seconds").isNotEqualTo(12);
        softly.assertThat(uptime.getSeconds()).as("seconds").isEqualTo(23);
        softly.assertAll();
    }

    @Test
    @DisplayName("Life time of the server is 44 minutes and 5 seconds")
    void shouldReturn44MinutesAnd5seconds() {
        when(timeStarter.getStartTimeOfServer()).thenReturn(Instant.parse("2021-12-15T00:44:05.00Z"));

        Uptime uptime = serverTime.getUptime();

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(uptime.getMinutes()).as("seconds").isEqualTo(44);
        softly.assertThat(uptime.getSeconds()).as("seconds").isEqualTo(5);
        softly.assertAll();
    }

    @Test
    @DisplayName("Life time of the server is 9 hours, 44 minutes and 5 seconds")
    void shouldReturn9hours44MinutesAnd5seconds() {
        when(timeStarter.getStartTimeOfServer()).thenReturn(Instant.parse("2021-12-15T09:44:05.00Z"));

        Uptime uptime = serverTime.getUptime();

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(uptime.getHours()).as("hours").isEqualTo(9);
        softly.assertThat(uptime.getMinutes()).as("minutes").isEqualTo(44);
        softly.assertThat(uptime.getSeconds()).as("seconds").isEqualTo(5);
        softly.assertAll();
    }

    @Test
    @DisplayName("Life time of the server is 4 days, 9 hours, 44 minutes and 5 seconds")
    void shouldReturn4days9hours44MinutesAnd5seconds() {
        when(timeStarter.getStartTimeOfServer()).thenReturn(Instant.parse("2021-12-19T09:44:05.00Z"));

        Uptime uptime = serverTime.getUptime();

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(uptime.getDays()).as("days").isEqualTo(4);
        softly.assertThat(uptime.getHours()).as("hours").isEqualTo(9);
        softly.assertThat(uptime.getMinutes()).as("minutes").isEqualTo(44);
        softly.assertThat(uptime.getSeconds()).as("seconds").isEqualTo(5);
        softly.assertAll();
    }
}