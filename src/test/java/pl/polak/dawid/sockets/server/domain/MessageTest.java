package pl.polak.dawid.sockets.server.domain;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatExceptionOfType;

class MessageTest {

    @Test
    @DisplayName("Message throw exception when body length is greater than 255 chars")
    void shouldThrowExceptionWhenMessageIsTooLongThan255Chars() {
        String body = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut " +
                "labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi " +
                "ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum " +
                "dolore eu fugiat nulla pariatur.";

        assertThat(body.length()).isGreaterThan(255);
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> new Message(1, body, 2));
    }
}
