package pl.polak.dawid.sockets.server.domain;

public class Message {

    private static final int MAX_LENGTH = 255;
    private Integer messageId;
    private Integer ownerId;
    private final String body;
    private final Integer senderId;


    public Message(Integer ownerId, String body, Integer senderId) {
        this.ownerId = ownerId;
        if (body.length() > MAX_LENGTH) {
            throw new IllegalArgumentException("Message to long. Max length is " + MAX_LENGTH + " chars. Be more" +
                                                " concise please ;)");
        }
        this.body = body;
        this.senderId = senderId;
    }

    public Message(Integer messageId, Integer ownerId, String body, Integer senderId) {
        this.messageId = messageId;
        this.ownerId = ownerId;
        this.body = body;
        this.senderId = senderId;
    }

    public Integer getMessageId() {
        return messageId;
    }

    public Integer getOwnerId() {
        return ownerId;
    }

    public Integer getSenderId() {
        return senderId;
    }

    public String getBody() {
        return body;
    }
}
