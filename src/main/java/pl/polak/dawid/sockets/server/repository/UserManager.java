package pl.polak.dawid.sockets.server.repository;

import pl.polak.dawid.sockets.server.domain.User;

import java.util.List;

public interface UserManager {

    boolean saveUser(User user);

    User loadUserByName(String userName);

    List<String> loadUsersList();

    void deleteUser(String userName);
}
