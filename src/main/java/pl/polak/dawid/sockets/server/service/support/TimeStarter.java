package pl.polak.dawid.sockets.server.service.support;

import java.time.Instant;

public class TimeStarter {

    public Instant getStartTimeOfServer() {return Instant.now();}
}
