package pl.polak.dawid.sockets.server.service;

import pl.polak.dawid.sockets.server.domain.Role;
import pl.polak.dawid.sockets.server.domain.User;
import pl.polak.dawid.sockets.server.repository.UserManagerBuilder;

import javax.security.auth.login.LoginException;
import java.util.List;

public class UserControl {

    public static User currentUser;
    private final UserManagerBuilder userBuilder;

    public UserControl(UserManagerBuilder userBuilder) {
        this.userBuilder = userBuilder;
    }

    public boolean userWantToLoginOrRegister() { return currentUser == null; }

    public boolean userExists(String userName) {
        List<String> users = userBuilder.loadUsersList();
        return users.contains(userName);
    }

    public boolean isAdmin() {
        return currentUser.getRole() == Role.ADMIN;
    }

    public boolean registerUser(String userName, String password) {
        User user = new User(userName, password, Role.USER);
        boolean userIsSaved = userBuilder.saveUser(user);


        if(userIsSaved) {
            currentUser = userBuilder.loadUserByName(userName);
        }

        return userIsSaved;
    }

    public void loginUser(String userName, String password) throws LoginException {
        if (userExists(userName)) {
            User user = userBuilder.loadUserByName(userName);

            if (password.equals(user.getPassword())) {
                currentUser = user;
            } else {
                throw new LoginException();
            }
        } else {
                throw new LoginException();
        }
    }

    public void logoutUser() {
        userBuilder.saveUser(currentUser);
        currentUser = null;
    }

    public void deleteUser(String userName) {
        if (!userExists(userName)) {
            throw new IllegalArgumentException("User " + userName +  " you want to delete does not exists in database");
        }
        userBuilder.deleteUser(userName);
    }
}
