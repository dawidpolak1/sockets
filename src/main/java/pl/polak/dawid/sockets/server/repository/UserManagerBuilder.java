package pl.polak.dawid.sockets.server.repository;

import org.jooq.DSLContext;
import org.jooq.Record1;
import org.jooq.Record4;
import org.jooq.Result;
import pl.polak.dawid.sockets.server.domain.Role;
import pl.polak.dawid.sockets.server.domain.User;

import java.util.List;
import java.util.stream.Collectors;

import static polak.dawid.sockets.server.repository.tables.Users.USERS;

public class UserManagerBuilder implements UserManager {

    private final DSLContext create;

    public UserManagerBuilder(DSLContext create) {
        this.create = create;

    }

    @Override
    public boolean saveUser(User user) {
        if (user.getUserId() == null) {
                int execute = create
                        .insertInto(USERS,
                                USERS.USERNAME, USERS.PASSWORD, USERS.ROLE)
                        .values(user.getUsername(), user.getPassword(), user.getRole().toString())
                        .execute();
                return execute > 0;
        } else {
            return false;
        }
    }

    @Override
    public User loadUserByName(String userName) {
        Record4<Integer, String, String, String> userRecord = create
                .select(USERS.USER_ID, USERS.USERNAME, USERS.PASSWORD, USERS.ROLE)
                .from(USERS)
                .where(USERS.USERNAME.eq(userName))
                .fetch()
                .get(0);

        return new User(
                userRecord.component1(),
                userRecord.component2(),
                userRecord.component3(),
                Role.valueOf(userRecord.component4())
        );
    }

    @Override
    public List<String> loadUsersList() {
        Result<Record1<String>> usersResult = create
                .select(USERS.USERNAME)
                .from(USERS)
                .fetch();

        return usersResult.stream()
                .map(userNameStringRecord -> userNameStringRecord.get(0, String.class))
                .collect(Collectors.toList());
    }

    @Override
    public void deleteUser(String userName) {
        create
                .delete(USERS)
                .where(USERS.USERNAME.eq(userName))
                .execute();
    }
}
