package pl.polak.dawid.sockets.server.domain;

public enum Role {

    ADMIN, USER
}
