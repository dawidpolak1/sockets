package pl.polak.dawid.sockets.server.service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import pl.polak.dawid.sockets.server.Server;
import pl.polak.dawid.sockets.server.domain.Message;
import pl.polak.dawid.sockets.server.repository.MessageManagerBuilder;
import pl.polak.dawid.sockets.server.repository.UserManagerBuilder;
import pl.polak.dawid.sockets.server.repository.DbConnector;
import pl.polak.dawid.sockets.server.service.support.Command;

import javax.security.auth.login.LoginException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.LinkedList;

public class ServerControl {

    private final Server server;
    private final PrintWriter out;
    private final BufferedReader in;
    private final UserControl userControl;
    private final MessageControl messageControl;


    public ServerControl(Server server) throws IOException {
        this.server = server;
        out = new PrintWriter(this.server.getClientSocket().getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(
                this.server.getClientSocket().getInputStream()));
        userControl = new UserControl(new UserManagerBuilder(new DbConnector().getContext()));
        messageControl = new MessageControl(new MessageManagerBuilder(new DbConnector().getContext()),
                                            new UserManagerBuilder(new DbConnector().getContext()));
    }

    private void sendCommand(boolean sendResponse, String body) {
        Command command = new Command(sendResponse, body);
        out.println(new Gson().toJson(command));
    }

    private void sendCommand(boolean sendResponse, String body, boolean stopConnection) {
        Command command = new Command(sendResponse, body, stopConnection);
        out.println(new Gson().toJson(command));
    }


    public void run() throws IOException {
        while (true) {
            showEntryPageToLoginOrRegister();
            if (userControl.isAdmin()) {
                adminMenu();
            } else {
                userMenu();
            }
        }
    }

    private void showEntryPageToLoginOrRegister() throws IOException {
        if (userControl.userWantToLoginOrRegister()) {
            String body = "\nEnter command:\n" +
                    "1 - login\n" +
                    "2 - register";

            boolean succeeded = true;
            while (succeeded) {
                sendCommand(true, body);
                switch (in.readLine()) {
                    case "1" -> succeeded =! login();
                    case "2" -> succeeded =! register();
                    default -> sendCommand(false, "There is no such command. Please try again");
                }
            }
        }
    }

    private boolean login() throws IOException {
        sendCommand(false, "Welcome! To log in enter user name and password ->");
        sendCommand(true, "User name:");
        String username = in.readLine();
        sendCommand(true, "Password:");
        String password = in.readLine();

        try {
            userControl.loginUser(username, password);
            sendCommand(false, "Login succeeded");
            return true;
        } catch (LoginException e) {
            sendCommand(false, "Login failed. Invalid user name" +
                    " or password");
            return false;
        }
    }

    private void logout() {
        userControl.logoutUser();
        sendCommand(false, "Logout succeeded, see you next time!");
    }

    private boolean register() throws IOException {
        String username;

        while (true) {
                sendCommand(true, "Welcome! For register, enter user name ->");
                username = in.readLine();
                if (!userControl.userExists(username)) {
                    break;
                }
                sendCommand(false, "User " + username + " already exists." +
                        " Select other user name.\n");
            }
            sendCommand(true, "Enter password ->");
            String password = in.readLine();
            boolean userIsRegistered = userControl.registerUser(username, password);
            sendCommand(false, "Registration " +
                    (userIsRegistered ? "succeeded" : "failed"));
            return userIsRegistered;
    }

    private void userMenu() throws IOException {
        String body = "\n Enter command:\n" +
                "1 - send/read/delete message\n" +
                "2 - logout";

        sendCommand(true, body);
        switch(in.readLine()) {
            case "1" -> messageMenu();
            case "2" -> logout();
            default -> sendCommand(false, "There is no such command. Please try again");
        }
    }

    private void messageMenu() throws IOException {
        boolean run = true;
        String body = "\nEnter command:\n" +
                "1 - send message\n" +
                "2 - read message from your inbox\n" +
                "3 - clear your inbox\n" +
                "4 - back";

        while (run) {
            sendCommand(true, body);
            switch (in.readLine()) {
                case "1" -> sendMessage();
                case "2" -> readMessages();
                case "3" -> deleteMessages();
                case "4" -> run = false;
                default -> sendCommand(false, "There is no such command. Please try again");
            }
        }
    }

    private void adminMenu() throws IOException {
            String body = "\n(ADMIN)\nEnter command:\n" +
                    "1 - server management\n" +
                    "2 - user management\n" +
                    "3 - logout";

            sendCommand(true, body);
            switch (in.readLine()) {
                case "1" -> serverManagement();
                case "2" -> userManagement();
                case "3" -> logout();
                default -> sendCommand(false, "There is no such command. Please try again");
            }
        }

    private void serverManagement() throws IOException {
        boolean run = true;
        String body = "\nEnter command:\n" +
                "1 - uptime\n" +
                "2 - info\n" +
                "3 - help\n" +
                "4 - stop servers\n" +
                "5 - back";

        while (run) {
            sendCommand(true, body);

            switch (in.readLine()) {
                case "1" -> uptime();
                case "2" -> info();
                case "3" -> help();
                case "4" -> {
                    run = false;
                    stop();
                }
                case "5" -> run = false;
                default -> sendCommand(false, "There is no such command. Please try again");
            }
        }
    }

    private void userManagement() throws IOException {
        boolean run = true;
        String body = "\nEnter command:\n" +
                "1 - send message\n" +
                "2 - read message from admin inbox\n" +
                "3 - read messages from given user\n" +
                "4- delete user\n" +
                "5 - back";

        while (run) {
            sendCommand(true, body);
            switch (in.readLine()) {
                case "1" -> sendMessage();
                case "2" -> readMessages();
                case "3" -> readMessagesFromOtherUser();
                case "4" -> deleteUser();
                case "5" -> run = false;
                default -> sendCommand(false, "There is no such command. Please try again");
            }
        }
    }



    private void sendMessage() throws IOException {
        String username;

        while (true) {
            sendCommand(true, "Send message to user ->");
            username = in.readLine();
            if (userControl.userExists(username)) {
                break;
            }
            sendCommand(false, "User " + username + " does not exists in database.\nTry again.");
        }
        sendCommand(true, "Enter message ->");
        String body = in.readLine();

        try {
            messageControl.sendMessage(username, body);
            sendCommand(false, "Sending succeeded");
        } catch (Exception e) {
            sendCommand(false, e.getMessage());
        }
    }

    private void readMessages() {
        try {
            LinkedList<Message> messages = messageControl.readUserMessages();
            sendCommand(false, new Gson().toJson(messages));
        } catch (IllegalStateException e) {
            sendCommand(false, e.getMessage());
        }
    }

    private void deleteMessages() throws IOException {
        boolean run = true;

        while (run) {
            sendCommand(true, "Do you want to clear your inbox?\ny/n");
            switch (in.readLine()) {
                case "y" -> {
                    messageControl.deleteMessagesFromInbox();
                    sendCommand(false, "Inbox cleared, you can receive messages again.");
                    run = false;
                }
                case "n" -> run = false;
                default -> sendCommand(false, "Enter y or n\n");
            }
        }
    }

    private void readMessagesFromOtherUser() throws IOException {
        String username;

        while(true) {
            sendCommand(true, "If you want to read messages from another user," +
                    " enter his user name ->");
            username = in.readLine();
            if (userControl.userExists(username)) {
                break;
            }
            sendCommand(false, "User " + username + " does not exists in database.\nTry again");
        }

        try {
            LinkedList<Message> userMessages = messageControl.readMessagesFromOtherUser(username);
            sendCommand(false, new Gson().toJson(userMessages));
        } catch (Exception e) {
            sendCommand(false, e.getMessage());
        }
    }

    private void  deleteUser() throws IOException {
        sendCommand(true, "Enter user name for delete account ->");
        String userName = in.readLine();

        try {
            messageControl.deleteMessages(userName);
            userControl.deleteUser(userName);
            sendCommand(false, "User " + userName + " has been deleted");
        } catch (Exception e) {
            sendCommand(false, e.getMessage());
        }
    }

    private void uptime() {
        String json = new Gson().toJson(server.getServerTime().getUptime());
        sendCommand(false, json);
    }

    private void info() {
        JsonObject response = new JsonObject();
        response.addProperty("serverVersion", server.getServerVersion());
        response.addProperty("creationDate", server.getStartDateOfServer().toString());
        sendCommand(false, response.toString());
    }

    private void help() {
        JsonObject response = new JsonObject();
        response.addProperty("uptime", Option.getUPTIME());
        response.addProperty("info", Option.getINFO());
        response.addProperty("help", Option.getHELP());
        response.addProperty("stop", Option.getSTOP());
        sendCommand(false, response.toString());
    }

    private void stop() {
        try {
            sendCommand(false,"Exit. Bye bye!", true);
            in.close();
            out.close();
            server.getClientSocket().close();
            server.getServerSocket().close();
        } catch (IOException exception) {
            System.err.println("Error while stopping connection");
        }
    }

    private static class Option {

        private static final String UPTIME = "1  - life time of the server";
        private static final String INFO = "2  - server version and creation date";
        private static final String HELP = "3 - list of available commands with description";
        private static final String STOP = "4 - stop both the server and the client";

        public static String getUPTIME() {
            return UPTIME;
        }

        public static String getINFO() {
            return INFO;
        }

        public static String getHELP() {
            return HELP;
        }

        public static String getSTOP() {
            return STOP;
        }
    }
}