package pl.polak.dawid.sockets.server.repository;

import org.jooq.DSLContext;
import org.jooq.Record4;
import org.jooq.Result;
import pl.polak.dawid.sockets.server.domain.Message;
import pl.polak.dawid.sockets.server.domain.Role;
import pl.polak.dawid.sockets.server.domain.User;
import polak.dawid.sockets.server.repository.tables.Messages;

import java.util.LinkedList;
import java.util.stream.Collectors;

import static polak.dawid.sockets.server.repository.tables.Messages.MESSAGES;

public class MessageManagerBuilder implements MessageManager {

    private final DSLContext create;

    public MessageManagerBuilder(DSLContext create) {
        this.create = create;
    }

    @Override
    public void saveMessage(Message message) {
        create
                .insertInto(MESSAGES,
                        MESSAGES.OWNER_ID, MESSAGES.BODY, MESSAGES.SENDER_ID)
                .values(message.getOwnerId(), message.getBody(), message.getSenderId())
                .execute();
    }

    @Override
    public LinkedList<Message> loadUserMessagesByUserId(int ownerId) {
        Result<Record4<Integer, Integer, String, Integer>> messageResult = create
                .select(MESSAGES.MESSAGE_ID, MESSAGES.OWNER_ID, MESSAGES.BODY, MESSAGES.SENDER_ID)
                .from(MESSAGES)
                .where(MESSAGES.OWNER_ID.eq(ownerId))
                .orderBy(MESSAGES.MESSAGE_ID)
                .fetch();

        return messageResult
                .stream()
                .map(record -> new Message(
                        record.component1(),
                        record.component2(),
                        record.component3(),
                        record.component4()))
                .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public void clearInbox(int ownerId) {
        create
                .delete(MESSAGES)
                .where(MESSAGES.OWNER_ID.eq(ownerId))
                .execute();
    }
}
