package pl.polak.dawid.sockets.server.service.support;

public class Uptime {

    private final int days;
    private final int hours;
    private final int minutes;
    private final int seconds;

    public Uptime(int days, int hours, int minutes, int seconds) {
        this.days = days;
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
    }

    public int getDays() {
        return days;
    }

    public int getHours() {
        return hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public int getSeconds() {
        return seconds;
    }
}
